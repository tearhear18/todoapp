'use strict';

/**
 * @ngdoc function
 * @name todoappApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the todoappApp
 */
angular.module('todoappApp')
  	.controller('IndexCtrl', [ "Task","$scope",function ( Task, $scope ) {
    	
        
    	$scope.todos = [];
    	$scope.task = {};
    	$scope.add_mode = true;

    	// Task.all( function(res){
     //        $scope.todos = res.data;
     //    });

        Task.all().then( function(res){
            console.log(res.data);
            $scope.todos = res.data;
        },  
        function(){
            alert("failed to connect to api");
        }
        )

    	$scope.delete_task = function( id , task_id ){
            console.log("asasas");
            Task.delete( task_id ).then(
                //success
                function(res){
                    console.log(res);
                },
                //fail
                function(){}
            );
    		$scope.todos.splice( id , 1 );
    	}
    	$scope.add_task = function( task ){
            Task.create( task ).then(
                //success
                function(res){
                    console.log(res);
                    if(res.data.status){
                        $scope.todos.push( res.data.payload );    
                    }else{
                        alert(res.data.payload);
                    }
                    
                },
                //fail
                function(){}
            )
    		
			$scope.task = {};
    	}

    	$scope.edit_task = function( id ){
    		$scope.task = $scope.todos[id];
    		$scope.add_mode = false;
    	}

    	$scope.update_task = function( task ){
    		console.log("ass");

            Task.update( task ).then(
                //success
                function(res){
                    $scope.add_mode =true;
                   console.log(res);
                   return false;
                },
                //fail
                function(){}

            );
           
    	}


    	
  }]);
