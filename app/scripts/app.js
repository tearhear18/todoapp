'use strict';

/**
 * @ngdoc overview
 * @name todoappApp
 * @description
 * # todoappApp
 *
 * Main module of the application.
 */
angular
  .module('todoappApp', ['ui.router'])

  .config(function( $stateProvider , $urlRouterProvider ){

	$urlRouterProvider.otherwise('/');

	$stateProvider
	    .state('index',{
	        url:'/',
	        templateUrl:'views/index.html',
	        controller:'IndexCtrl'
	    })
	    
	    ;

})
