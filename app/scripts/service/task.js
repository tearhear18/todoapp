'use strict';

/**
* @ngdoc function
* @name ranOnRailsApp.controller:MainCtrl
* @description
* # MainCtrl
* Controller of the ranOnRailsApp
*/
angular.module('todoappApp')
.service('Task',['$http', function( $http ){
    
    var f = {};
    var url = "http://localhost:3000"
    
    f.all = function( k ){
       return $http.get(url+"/task");
    }
    f.delete = function( _id ){
    	
    	return $http.post(url+"/task/delete",{id:_id});
    }
    f.create = function( _task ){
    	return $http.post(url+"/task",_task)
    }
    f.update = function( task ){
    	return $http.post(url+"/task/update",task)
    }
    return f;

}]);
